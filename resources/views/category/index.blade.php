@extends('layouts.master')

@section('title', 'Category ')

@section('top-styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@endsection

@section('content')
@section('breadcrumb','Menu Category ')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i> Menu Category 
                            <a href="{{route('category.create')}}" class="col-md-2 float-sm-right">
                                <button type="button" class="btn btn-block btn-primary btn-md">Add Menu Category </button>
                            </a>
                        </div>
                        <div class="panel-body">
                            <table id="DataTable" class="table table-bordered table-hover table-responsive" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort text-center" width="5%">S.No</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th class="no-sort text-center" width="12%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($cat as $c)
                                        <tr>
                                            <td>
                                                {{$i++}}
                                            </td>
                                            <td>
                                                <img src="{{url('').'/uploads/'.$c->image}}" height="50px">
                                            </td>
                                            <td>
                                                {{$c->name}}
                                            </td>
                                            <td>
                                                <a href="{{route('category.edit',$c->id)}}" class="text-info p-1">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
<!-- ./wrapper -->
@section('page-scripts')
<!-- DataTables -->
<script src="{{url('')}}/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#DataTable').DataTable();
    });

</script>
@endsection
@endsection
