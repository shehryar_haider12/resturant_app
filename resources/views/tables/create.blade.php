@extends('layouts.master')

@section('title', 'Add Tables')

@section('top-styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/select2/css/select2.min.css">
 <style>
   .mt30 {
    margin-top: 35px;
    margin-left: 20px;
}
 </style>
 @endsection

@section('content')
    @section('breadcrumb','Add Tables')
    {{-- {{dd($laboratory_test->laboratory)}} --}}
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- jquery validation -->
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i> Add Tables
                              <button onclick="window.history.back(1)" type="button" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Go Back</button>
                        </div>
                        <div class="panel-body">
                          <!-- form start -->
                          <form action="{{$isEdit ? route('tables.update',$table->id) : route('tables.store')}}" role="form" method="POST" id="quickForm" enctype="multipart/form-data">
                            @csrf
                            @if ($isEdit)
                              <input type="hidden" name="_method" value="put">
                            @endif
                            <div class="row">
                              
                              <div class="form-group col-md-4">
                                <label for="fee">Table Number</label>
                                <input type="text" name="table_no" class="form-control"  placeholder="Enter Table Number" value="{{$table->table_no ?? old('table_no')}}">
                                <span class="text-danger">{{$errors->first('table_no') ?? null}}</span>

                              </div>
                            </div>
                              <div class="card-footer">
                                  <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                          </form>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
  
            </div>
            <!--/.col (right) -->
          </div>
        
    </div>
  <!-- ./wrapper -->
@section('page-scripts')
<!-- jquery-validation -->
<script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/assets/plugins/jquery-validation/additional-methods.min.js"></script>
<script src="{{url('')}}/assets/plugins/select2/js/select2.min.js"></script>
@endsection

@endsection