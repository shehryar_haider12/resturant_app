@extends('layouts.master_web')
@section('content')
 
<section id="order_detail_page_top">
    <p class="text-center"><a href="{{route('scan')}}"><i class="fa fa-chevron-left"></i> &nbsp;Main</a></p>

<div class="container">
    <div class="row pd_top">


        <div class="col-md-6">

    <img src="{{url('')}}/web_assets/images/Group 561.png" class="center" width="400px" height="400px">

</div>

        <div class="col-md-6">
            <div class="detail_box_for_order_sum">
                <p class="title">Order Details</p>
                <p class="detail_summary_view">Order No  <span style="float: right;">{{$order_details->order_no}}</span></p>
                <p class="detail_summary_view">Table No  <span style="float: right;">{{$order_details->table_id}}</span></p>
                <p class="detail_summary_view">Amount  <span style="float: right;">RS. {{$order_details->total_amount}}</span></p>
                <p class="detail_summary_view">Date  <span style="float: right;">{{$order_details->created_at->format('F d Y')}}</span></p>
                <p class="detail_summary_view">Status  <span style="float: right;">Processig</span></p>



            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="total_bar_box">

                        <p class="total_bar">Total  <span style="float: right;">Rs. {{$order_details->total_amount}}</span></p>
                    </div>
                  <div class="row" style="flex: auto; padding: 16px;">

                    <p class="cd_card"><img src="{{url('')}}/web_assets/images/icons/credit-card (1).svg" width="30px" height="30px"> &nbsp;<span> Click Here to pay online</span></p>&nbsp;


                      &nbsp;&nbsp;<p class="cd_card"><img src="{{url('')}}/web_assets/images/icons/credit-card (1).svg" width="30px" height="30px"> &nbsp;<span> Click Here to pay online</span></p>

                  </div>
                </div>

                <div class="row "style="flex: auto;">

                    <div class="col-md-5 offset-2">   <button type="button" class="btn btn-primary view_order_btn"><img src="{{url('')}}/web_assets/images/icons/clock (1).svg" width="20px" height="20px">  &nbsp;View order time</button></div>
                    <div class="col-md-5  offset-2">   <button type="button" class="btn btn-primary feedback_btn"><img src="{{url('')}}/web_assets/images/icons/comment.svg" width="20px" height="20px">  &nbsp;Feed back</button></div>


                </div>


</div>



</div>
</div>
</section>
@endsection