@extends('layouts.master_web')

@section('content')
   
<form action="{{route('cart.add_items')}}" method="post">
    @csrf
<section id="radio_selection_page_top">
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <button onclick="window.history.back(1)" type="button" class="btn btn-dark goback_btn">Go back</button>
        </div>
        <div class="col-md-4">
            <img src="{{url('')}}/web_assets/images/kfc_PNG53.png" width="220px">

        </div>


    </div>
</div>

</section>
@php
    $adons = adon_items();
@endphp
<section id="details_banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{url('')}}/uploads/{{$item->image}}" width="100%">
            </div>

            <div class="col-md-6 detail_meals">
                <h2>{{$item->name}}</h2>
                <h4>RS. {{$item->price}}</h4>
                <input type="hidden" class="price" value="{{$item->price}}">
                <p>{{$item->description}} </p>

            </div>

        </div>
    </div>
    <input type="hidden" name="item_id[]" value="{{$item->id}}">

</section>
<section id="collap_meals_sec">
    <div class="container">
        <div class="row ">
  <!--Meal-->
  {{-- {{dd($item->sections)}} --}}
                @foreach ($item->sections as $section)
                <div class="col-md-7 offset-md-2">

                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne{{$section->id}}">
                                <h5 class="mb-0">
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne{{$section->id}}" aria-expanded="true" aria-controls="collapseOne{{$section->id}}" style="text-transform: uppercase;">
                                        {{$section->items->first()->type}} <span><i class="fas fa-chevron-down"></i></span>
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne{{$section->id}}" class="collapse " aria-labelledby="headingOne{{$section->id}}"
                                data-parent="#accordion">
                                <div class="card-body">

                                    @foreach ($section->items as $child_item)
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="customcheck">
                                                <p style="font-size: 20px;"> {{$child_item->name}}</p>
                                                <input type="checkbox" class="{{$section->is_paid == 1 ? 'get-price' : null}}" name="item_id[]" id="{{$section->id}}" value="{{$child_item->id}}" data-price="{{$child_item->price}}">
                                                <span class="checkmark"> </span>
                                            </label>
                                        </div>
                                        {{-- {{dd($section)}} --}}
                                        @if ($section->is_paid == 1)
                                        <div class="col-md-3">
                                                <p style="color: #c4122f;font-weight: bold;">{{!empty($child_item->price) ? 'RS. '.$child_item->price : null}}</p>
                                                <input type="hidden" class="sub_price" value="{{$child_item->price}}">
                                        </div>
                                        <div class="col-md-3">
                                            {{-- <div id="field1">
                                                <div class="qty ">
                                                    <span class="minus1 bg-dark1">-</span>
                                                    <span class="plus1 bg-dark1">+</span>
                                                </div>
                                            </div> --}}
                                            <input type="text" name="description" class="form-control"  placeholder="Enter Item Description" value="1">


                                        </div>
                                        @endif
                                        
                                    </div>
                                    @endforeach

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                
            @endforeach
            {{-- {{dd($adons)}} --}}
            <div class="col-md-7 offset-md-2">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne{{$adons->first()->type}}">
                            <h5 class="mb-0">
                                <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne{{$adons->first()->type}}" aria-expanded="true" aria-controls="collapseOne{{$adons->first()->type}}" style="text-transform: uppercase;">
                                    {{$adons->first()->type}} <span><i class="fas fa-chevron-down"></i></span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne{{$adons->first()->type}}" class="collapse " aria-labelledby="headingOne{{$adons->first()->type}}"
                            data-parent="#accordion">
                            <div class="card-body">

                                @foreach ($adons as $ch_item)
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="customcheck">
                                            <p style="font-size: 20px;">{{$ch_item->name}}</p>
                                            <input type="checkbox" class="{{$section->is_paid == 1 ? 'get-price' : null}}" name="item_id[]" id="{{$adons->first()->type}}" value="{{$ch_item->id}}" data-price="{{$ch_item->price}}">
                                            <span class="checkmark"> </span>
                                        </label>
                                    </div>
                                    {{-- {{dd($section)}} --}}
                                    @if ($section->is_paid == 1)
                                    <div class="col-md-3">
                                            <p style="color: #c4122f;font-weight: bold;">{{!empty($ch_item->price) ? 'RS. '.$ch_item->price : null}}</p>
                                            <input type="hidden" class="sub_price" value="{{$ch_item->price}}">
                                    </div>
                                    <div class="col-md-3">
                                        {{-- <div id="field1">
                                            <div class="qty ">
                                                <span class="minus1 bg-dark1">-</span>
                                                <span class="plus1 bg-dark1">+</span>
                                            </div>
                                        </div> --}}
                                        <input type="text" name="description" class="form-control"  placeholder="Enter Item Description" value="1">


                                    </div>
                                    @endif
                                    
                                </div>
                                @endforeach

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            </div>

        </div>

        </div>
    </div>
</section>


<section id="addtocart">
  <div class="container">
  <div class="row">


      <div class="col-md-2 offset-md-2">

          <div class="qty ">
              <span class="minus bg-dark">-</span>
              <input type="number" class="count" name="quantity" value="1">
              <span class="plus bg-dark">+</span>
          </div>
      </div>

<div class="col-md-2">
    <h3 style="visibility: hidden">.</h3>
    <br>
    <button type="submit" class="btn btn-lg btn-primary addtocart_btn">Add to cart</button>
</div>
<div class="col-md-3">
    <h3 style="visibility: hidden">.</h3>
    <br>
    <span class="total_text">Total: RS.</span>  <span class="total">{{$item->price}}</span>
</div>

  </div>
  </div>
</section>
</form>


@section('custom-script')

<script>
    $(document).ready(function(){
    // $('.count').prop('disabled', true);
    $(document).on('click','.plus',function(){
        $('.count').val(parseInt($('.count').val()) + 1 );
        console.log($('.count').val());
    });
    $(document).on('click','.minus',function(){
        $('.count').val(parseInt($('.count').val()) - 1 );
        if ($('.count').val() == 0) {
            $('.count').val(1);
        }
    });
 
    $(document).on('click','.plus1',function(){
        $('.count1').val(parseInt($('.count1').val()) + 1 );
        console.log($('.count1').val());
    });
    $(document).on('click','.minus1',function(){
        $('.count1').val(parseInt($('.count1').val()) - 1 );
        if ($('.count1').val() == 0) {
            $('.count1').val(1);
        }
    });

    $('.get-price').click(function(){
        if($(this).is(':checked'))
        {
            var item_price = $(this).data('price');
            var price = $('.total').html();
            var total = Number(item_price) + Number(price);
            $('.total').html(total);
        }
       else{
        var item_price = $(this).data('price');
        var price = $('.total').html();
        var total = Number(price) - Number(item_price);
        $('.total').html(total);
       }
    });
});
</script>
@endsection
@endsection