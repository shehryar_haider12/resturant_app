<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{url('')}}/web_assets/vendors/bootstrap/css/bootstrap.min.css" media="all">
    <link rel="stylesheet" href="{{url('')}}/web_assets/vendors/fontawesome/css/all.min.css">
    <style>
        #preview{
        width:100%;
        height: 800px;
        margin:0px auto;
        }
        .btn-group.btn-group-toggle.mb-5 {
            position: relative;
            left: 450px;
            top: 20px;
        }
    </style>
    <script src="{{url('')}}/web_assets/vendors/jquery.min.js"></script>
    {{-- Axios --}}
    <script src="{{url('')}}/assets/custom/js/axios.min.js"></script>

</head>
<body>
    
<div class="container">
    <video id="preview"></video>
    <div class="btn-group btn-group-toggle mb-5" data-toggle="buttons">
        <label class="btn btn-primary active">
          <input type="radio" name="options" value="1" autocomplete="off" checked> Front Camera
        </label>
        <label class="btn btn-secondary">
          <input type="radio" name="options" value="2" autocomplete="off"> Back Camera
        </label>
      </div>
</div>
{{-- <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script> --}}
<script src="{{url('')}}/js/scan.min.js"></script>
<script type="text/javascript">
    var scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 5, mirror: false });
    scanner.addListener('scan',function(content){
        // alert(content);
                var table_id = content;
                    axios
                    .post('{{route("store.table")}}', {
                        _method: 'post',
                        _token: '{{csrf_token()}}',
                        table_id: table_id,
                    })
                    .then(function (response) {
                        console.log(response);
                        window.location.href = '{{route("web_index")}}';
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            
        //window.location.href=content;
    });
    Instascan.Camera.getCameras().then(function (cameras){
        if(cameras.length>0){
            scanner.start(cameras[0]);
            $('[name="options"]').on('change',function(){
                if($(this).val()==1){
                    if(cameras[0]!=""){
                        scanner.start(cameras[0]);
                    }else{
                        alert('No Front camera found!');
                    }
                }else if($(this).val()==2){
                    if(cameras[1]!=""){
                        scanner.start(cameras[1]);
                    }else{
                        alert('No Back camera found!');
                    }
                }
            });
        }else{
            console.error('No cameras found.');
            alert('No cameras found.');
        }
    }).catch(function(e){
        console.error(e);
        alert(e);
    });
</script>
</body>
</html>
