@extends('layouts.master_web')
@section('content')
    
<div class="container">
    <div class="row">
<div class="col-md-12 shadow">
    <nav class="navbar navbar-expand-md navbar-dark ">
        <a class="navbar-brand" href="#">
            <img src="{{url('')}}/web_assets/images/kfc_PNG53.png" width="220px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                @foreach ($categories as $category)

                    <li class="nav-item">
                        <a class="nav-link 
                        @if(!empty($category_detail)) 
                            {{$category_detail == $category->id ? 'active' : null}}" href="{{route('category_items',$category->id)}}
                        @endif">{{$category->name}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </nav>
    </div>
</div>
</div>
<section id="top_search_btn">
    <!--<top bar start>-->
    <div class="container">

        <div class="row" style="padding-top: 40px">
            <div class="col-md-4">
                <button onclick="window.history.back(1)" type="button" class="btn btn-primary btn_goback">Go Back</button>
            </div>

            <div class="col-md-4">
                @if(!empty($category_detail)) 
                    <form action="{{route('category_items',$category_detail)}}" method="get">
                @else
                    <form action="{{route('category_items')}}" method="get">
                @endif
                    
                    <div class="searchbar">
                        <input class="search_input" type="text" name="keyword" placeholder="Search...">
                        {{-- <a href="#" class="search_icon"><i class="fas fa-search"></i></a> --}}
                        <button type="submit" class="search_icon" style="background: none;border: none;width: 100px;cursor: pointer;visibility: hidden;"> 
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </form>
            </div>

        </div>


    </div>
    <!--<top bar end>-->
</section>

<section id="add_to_cart_box">

    <div class="container">
        <div class="row pd">
            @if ($category_item->count() > 0)
                
            @foreach ($category_item as $item)   
            <div class="col-md-4">
                <a href="{{route('item.detail',$item->id)}}">
                    <div class="card">
                        <img class="card-img-top" src="{{url('')}}/uploads/{{$item->image}}"
                            alt="Card image cap">
                        <div class="card-body">

                            <h5 class="card-title">{{$item->name}}
                            <span class="price">RS. {{$item->price}} </span></h5>
                            <h4 class="price"> </h4>
                            <p class="card-text">{{$item->description}}</p>
                            <!--<span class="btn btn-primary addtocart_btn"> <a href="#" ><h5>view</h5>Add to Cart </a></span>-->
                           
                                <center>
                                <div class="btn_border_row row col-md-12" style=" border: 3px solid #c4122f;padding: 0px">
                                    <a class="col-lg-6 col-md-12 col-sm-12" style="padding: 0px" href="{{route('item.detail',$item->id)}}">
                                        <button type="button" style="padding-right: 0px" class="btn btn-primary btn_view col-md-12">View Detail</button>
                                    </a>
                                    <a class="col-lg-6 col-md-12 col-sm-12" style="padding: 0px" href="{{route('cart.add',$item->id)}}">
                                        <button type="button" style="padding-right: 0px" class="btn btn-light addto_cart col-md-12">Add to Cart</button>
                                    </a>
                                </div>
                            </center>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            @else
            <div class="col-md-12">
                <p class="alert alert-warning">No Item Found !</p>
            </div>
            @endif
            
        </div>

        {{-- <div class="row pd">
            @foreach ($category_item as $item)   
                <div class="col-md-4">
                    <a href="{{route('item.detail',$item->id)}}">
                        <div class="card">
                            <img class="card-img-top" src="{{url('')}}/uploads/{{$item->image}}" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">{{$item->name}}</h5>
                                <h4 class="price"> RS. {{$item->price}}  </h4>
                                <p class="card-text">{{$item->description}}.</p>
                                <p class="text-center"> <a href="{{route('cart.add',$item->id)}}" class="btn btn-primary addtocart_btn">Add to Cart</a></p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach

        </div> --}}

    </div>
</section>
<section id="view_cart_sec">
    <div class="container">
        <div class="row pd">
     <div class="col-md-6 offset-md-3">
         <a href="{{route('cart')}}">
            <div class="box">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">View Your Cart - <span> Rs.{{ \Cart::session(1)->getSubTotal()}}</span>
                    </div>
   
   
                </div>
   
            </div>
         </a>
     </div>

        </div>
    </div>
</section>
@section('custom-script')
<script>
    $(document).on("scroll", function(){
        if
        ($(document).scrollTop() > 86){
            $("#banner").addClass("shrink");
        }
        else
        {
            $("#banner").removeClass("shrink");
        }
    });
</script>
@endsection
@endsection