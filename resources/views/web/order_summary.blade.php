@extends('layouts.master_web')
@section('content')
<form action="{{route('order.store')}}" method="post"> 
    @csrf
<section id="top_logo_order_sum">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <button onclick="window.history.back(1)" type="button" class="btn btn-dark goback_btn" style="background-color:#c3112e    margin-top: 32px;padding: 8px 48px;background: #c4122f;border-radius: 17px;border: none;">Go back</button>
            </div>
         <div class="col-md-12">
            <p class="text-center"><img src="{{url('')}}/web_assets/images/kfc_PNG53.png" width="200px" height="100px"></p>
         </div>
         </div>
    </div>

</section>
<section id="order_sum">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">

            <div class="sumary_box">
                <p class="head">Order Details<br></p>
                @if ($cart->count() > 0)
                    
                @foreach ($cart as $cart_item)
                <input type="hidden" name="item_id[]" value="{{$cart_item->id}}">
                <input type="hidden" name="quantity[]" value="{{$cart_item->quantity}}">
                <input type="hidden" name="price[]" value="{{$cart_item->price}}">
                    <div class="row pd">
                        <div class="col-md-2"> <h6 class="counter">{{$cart_item->quantity}}</h6> </div>

                            <div class="col-md-4 ">
                                <div class="row text-muted">{{$cart_item->name}}</div>

                            </div>
                        <div class="col-md-4 offset-2 price_tag"> Rs.{{$cart_item->price}} <a href="{{route('cart.delete',$cart_item->id)}}"><span class="close">&#10005;</span></a></div>
                    </div>
                @endforeach
                @else
                <div class="row pd ">
                    <p class="alert alert-danger col-md-12" style="color: #721c249e;font-size: 15px;">Cart is Empty</p>
                </div>
                @endif
            </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="total_bar">
                            <p class="total_title">Total<span style="float: right">Rs.{{ \Cart::session(1)->getSubTotal()}}</span></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12"><p class="text-center"> <button type="submit" class="btn btn-primary place_order">Place Order</button></p></div>

            </div>
        </div>
    </div>
</section>
</form>
@endsection