@extends('layouts.master_web')
@section('content')
    
<section id="top_bar">
<!--<top bar start>-->
<div class="container">

    <div class="row">

            <div class="tabbg col-md-2 offset-md-2 "></div>
            <div class="tabbg col-md-2 offset-md-1  "></div>
            <div class="tabbg col-md-2 offset-md-1 "></div>

    </div>

</div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="{{url('')}}/web_assets/images/kfc_PNG53.png" height="150px" width="200px">
            </div>
           <div class="col-md-5">
            <form action="{{route('category_items')}}" method="get">
                <div class="searchbar">
                    <input class="search_input" type="text" name="keyword" placeholder="Search..." >
                    <button type="submit" class="search_icon" style="background: none;border: none;width: 100px;cursor: pointer;visibility: hidden;">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </form>
           </div>

        </div>
    </div>
    <!--<div class="row" style="padding-top: 40px">-->
        <!--<div class="col-md-4">-->
            <!--<img src="{{url('')}}/web_assets/images/kfc_PNG53.png" width="220px">-->
        <!--</div>-->

        <!--<div class="col-md-4">-->
            <!--<div class="searchbar">-->
                <!--<input class="search_input" type="text" name="" placeholder="Search..." >-->
                <!--<a href="#" class="search_icon"><i class="fas fa-search"></i></a>-->

            <!--</div>-->
        <!--</div>-->


        <!--</div>-->




<!--<top bar end>-->
</section>

<section id="main_slider">
    <div id="demo" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{url('')}}/web_assets/images/Mask%20Group%2033.png" width="100%" height="auto" alt="Los Angeles">
            </div>
            <div class="carousel-item ">
                <img src="{{url('')}}/web_assets/images/Mask%20Group%2033.png" width="100%" height="auto"  alt="Los Angeles">
            </div>

        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span><i class="fa fa-chevron-left"></i></span>

        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span><i class="fa fa-chevron-right"></i></span>
        </a>

    </div>

</section>

<section id="cards_menu">
    <div class="container">
     <div class="row">
        @foreach ($categories as $category)
            <div class="col-md-4" style="margin-bottom:15px ">
                <a href="{{route('category_items',$category->id)}}">
                    <div class="card">
                        <img class="card-img-top" src="{{url('')}}/uploads/{{$category->image}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$category->name}}</h5>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
            
        
         {{-- <div class="col-md-4">
             <div class="card">


                 <img class="card-img-top" src="{{url('')}}/web_assets/images/500e9665b83a2ac66115d56b3cf1ead7.png" alt="Card image cap">
                 <div class="card-body">
                     <h5 class="card-title">Everyday Value</h5>
                 </div>
             </div>
         </div>
         <div class="col-md-4">
             <div class="card">


                 <img class="card-img-top" src="{{url('')}}/web_assets/images/500e9665b83a2ac66115d56b3cf1ead7.png" alt="Card image cap">
                 <div class="card-body">
                     <h5 class="card-title">Everyday Value</h5>
                 </div>
             </div>
         </div> --}}


     </div>

    </div>
</section>
<section id="gift_modal">
<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Gift box
</button> --}}

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="ii">&times;</span>
                </button>
            </div>
            <div class="modal-body ">
                <div class="col-md-4 bg offset-md-3"></div>
                <button type="button" class="btn btn-secondary open offset-4" data-dismiss="modal">Open Now</button>

            </div>

            <!--<div class="modal-footer">-->
            <!--</div>-->
        </div>
    </div>
</div>
</section>


<section id="discount">
    <!-- Button trigger modal -->
    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#discount_Modal">
       discount
    </button> --}}

    <!-- Modal -->
    <div class="modal fade" id="discount_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-md-4  offset-md-4" style=" padding: 28px;"><img src="{{url('')}}/web_assets/images/icons/discount.svg" width="100px" height="100px" ></div>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="ii">&times;</span>
                    </button>
                </div>
                <div class="modal-body ">
                    <h2 class="title">Refuel Your Appetite</h2>
                    <h1 class="des">10% discounts</h1>
                    <button type="button" class="btn btn-secondary open offset-4" data-dismiss="modal">Register Now</button>

                </div>


            </div>
        </div>
    </div>
</section>

<section id="before_you_go">
    <!-- Button trigger modal -->
    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#before_Modal">
        before you go
    </button> --}}

    <!-- Modal -->
    <div class="modal fade" id="before_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="col-md-12  " ><img src="{{url('')}}/web_assets/images/icons/XMLID_1030_.svg" width="50px" height="50px" >
                        <h3 class="title">Before You Go</h3>
                        <p class="des">Bless us with<br>
                            your feedback and get a</p>
                        <h6 class="text-center blow_title">Free Brownie</h6>
                        <img src="{{url('')}}/web_assets/images/Mask Group 29.png"  width="300px" height="200px">

                    </div>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="ii">&times;</span>
                    </button>
                </div>
                <div class="modal-body ">
                    <button type="button" class="btn btn-secondary open offset-4" data-dismiss="modal">Let,s do it</button>

                </div>


            </div>
        </div>
    </div>
</section>

@endsection
