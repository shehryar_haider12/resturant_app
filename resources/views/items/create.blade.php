@extends('layouts.master')

@section('title', 'Add Menu Item')

@section('top-styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/select2/css/select2.min.css">
 <style>
   .mt30 {
    margin-top: 35px;
    margin-left: 20px;
  }
  li.select2-selection__choice {
      color: #6d6a6a !important;
  }
  span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }
    i.fa.fa-minus {
        /* float: right; */
        position: relative;
        padding: 8px;
        background: #ff5722;
        color: white;
        cursor: pointer;
    }
    .fa-plus {
    position: relative;
    padding: 8px;
    background: #8BC34A;
    color: white;
    cursor: pointer;
    }
    i#add-court {
        left: 135px;
    }
    i.fa.fa-minus.remove-court {
        left: 140px;
    }


 </style>
 @endsection

@section('content')
    @section('breadcrumb','Add Menu Item')
    {{-- {{dd($laboratory_test->laboratory)}} --}}
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- jquery validation -->
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i> Add Menu Item
                              <button onclick="window.history.back(1)" type="button" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Go Back</button>
                        </div>
                        <div class="panel-body">
                          <!-- form start -->
                          <form action="{{$isEdit ? route('items.update',$item->id) : route('items.store')}}" role="form" method="POST" id="quickForm" enctype="multipart/form-data">
                            @csrf
                            @if ($isEdit)
                              <input type="hidden" name="_method" value="put">
                            @endif
                            <div class="row">
                              <div class="col-md-4">
                                  <div class="avatar-upload" style="height: 200px;">
                                      <div class="avatar-edit">
                                          <input type='file' name="image" id="imageUpload1"
                                              accept=".png, .jpg, .jpeg" />
                                          <label for="imageUpload1"><span>{{ __('Featured image')}} </span></label>
                                      </div>
                                      <div class="avatar-preview">
                                          <div id="imagePreview1"
                                              style="background-image : url({{$isEdit ? url('').'/uploads/'.$item->image : url('').'/uploads/placeholder.jpg'}}">
                                          </div>
                                      </div>
                                  </div>
                                  <span class="text-danger">{{$errors->first('image') ?? null}}</span>
                              </div>
                              <div class="col-md-8">
                                <div class="form-group" style="margin-bottom: 35px">
                                  <label for="name">Select Category</label>
                                  <select name="c_id" class="form-control"> 
                                    <option value="" selected disabled>Select Category</option>
                                    @foreach ($cat as $c)
                                        <option value="{{$c->id}}" 
                                        @if ($isEdit)
                                            {{$item->c_id == $c->id ? 'selected' : null}}
                                        @endif>{{$c->name}} </option>
                                    @endforeach
                                  </select>
                                  <span class="text-danger">{{$errors->first('c_id') ?? null}}</span>
                                </div>
                                <div class="form-group">
                                  <label for="fee">Item Name</label>
                                  <input type="text" name="name" class="form-control"  placeholder="Enter Item Name" value="{{$item->name ?? old('name')}}">
                                  <span class="text-danger">{{$errors->first('name') ?? null}}</span>

                                </div>
                                
                              </div>
                            </div>
                            <br>
                              <div class="row">
                              <div class="form-group col-md-12">
                                <label for="fee">Price</label>
                                <input type="text" name="price" class="form-control"  placeholder="Enter Item Price" value="{{$item->price ?? old('price')}}">
                                <span class="text-danger">{{$errors->first('price') ?? null}}</span>

                              </div>
                              <div class="form-group col-md-12">
                                <label for="fee">Description</label>
                                <input type="text" name="description" class="form-control"  placeholder="Enter Item Description" value="{{$item->description ?? old('description')}}">
                                <span class="text-danger">{{$errors->first('description') ?? null}}</span>

                              </div>
                              <div class="form-group col-md-12">
                                <h3 style="font-weight: bold;margin-bottom:0px" for="add_detail">Add Detail </h3> <span class="text-danger">Note: If you dont want to add details then leave section empty</span>
                                <br>
                                <br>
                                <i class="fa fa-plus" id="add"></i>
                                <i class="fa fa-minus remove"></i>
                              </div>
                              <div id="prize-section" class="col-md-12">
                                @if ($isEdit)
                                @php
                                    $i=1;
                                @endphp
                                @foreach ($item->sections as $key => $section)
                                {{-- {{dd($section->singleItem)}} --}}
                                  <div class="row prize col-md-12">
                                    <div class="form-group col-md-6">
                                      <label for="name">Select Section</label>
                                      <select name="section_id[]" id="1" class="form-control section_id"> 
                                        <option value="{{$section->singleItem->id}}">{{$section->singleItem->name}}
                                       </option>

                                      </select>
                                      <span class="text-danger">{{$errors->first('section_id') ?? null}}</span>
                                    </div>
                                    <div class="form-group col-md-6">
                                      <label for="name">Select Items</label>
                                      <select name="is_paid[]" id="is_paid" class="form-control">
                                            <option value="1" {{$section->is_paid == 1 ? 'selected' : null}}>Paid</option>
                                            <option value="0" {{$section->is_paid == 0 ? 'selected' : null}}>Free</option>
                                      </select>
                                      <span class="text-danger">{{$errors->first('is_paid') ?? null}}</span>
                                    </div>
                                  </div>
                                @endforeach
                                @else
                                  <div class="row prize col-md-12">
                                    <div class="form-group col-md-6">
                                      <label for="name">Select Section</label>
                                      <select name="section_id[]" id="1" class="form-control section_id"> 
                                        <option value="" selected disabled>Select Section</option>

                                        @foreach ($sections as $key=> $section)
                                            <option value="{{$section->id}}">{{$section->name}} </option>
                                        @endforeach
                                      </select>
                                      <span class="text-danger">{{$errors->first('section_id') ?? null}}</span>
                                    </div>
                                    <div class="form-group col-md-6">
                                      <label for="name">Amount</label>
                                      <select name="is_paid[]" id="is_paid" class="form-control">
                                            <option value="1">Paid</option>
                                            <option value="0">Free</option>
                                      </select>
                                      <span class="text-danger">{{$errors->first('is_paid') ?? null}}</span>
                                    </div>
                                  </div>
                                    
                                @endif
                              </div>
                            </div>
                            </div>
                              <div class="card-footer">
                                  <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                          </form>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
  
            </div>
            <!--/.col (right) -->
          </div>
        
    </div>
  <!-- ./wrapper -->
@section('page-scripts')
<!-- jquery-validation -->
<script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/assets/plugins/jquery-validation/additional-methods.min.js"></script>
<script src="{{url('')}}/assets/plugins/select2/js/select2.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
    
      $('.select').select2();

      $('#quickForm').validate({
        rules: {
          name: { required: true, },
          description: { required: true, },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });

      function readURL(input, number) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview' + number).hide();
                $('#imagePreview' + number).fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
          }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });

    $('#add').click(function(){
      var id = $('.section_id').last().attr('id');
      id  = id+1; 
      $('#prize-section').append(`
        <div class="row prize col-md-12">
          <div class="form-group col-md-6">
            <label for="name">Select Section</label>
            <select name="section_id[]" id="1" class="form-control section_id"> 
              <option value="" selected disabled>Select Section</option>

              @foreach ($sections as $key=> $section)
                  <option value="{{$section->id}}">{{$section->name}} </option>
              @endforeach
            </select>
            <span class="text-danger">{{$errors->first('section_id') ?? null}}</span>
          </div>
          <div class="form-group col-md-6">
            <label for="name">Amount</label>
            <select name="is_paid[]" id="is_paid" class="form-control">
                  <option value="1">Paid</option>
                  <option value="0">Free</option>
            </select>
            <span class="text-danger">{{$errors->first('is_paid') ?? null}}</span>
          </div>
        </div>
        `);
    });
    $('.remove').click(function(){
      $('#prize-section .prize:last-child').remove();
    });
  });
</script>
@endsection
@endsection