@extends('layouts.master')

@section('title', 'Add '.$type)

@section('top-styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
 <link rel="stylesheet" href="{{url('')}}/assets/plugins/select2/css/select2.min.css">
 <style>
   .mt30 {
    margin-top: 35px;
    margin-left: 20px;
  }
  li.select2-selection__choice {
      color: #6d6a6a !important;
  }
  span.fa.fa-close {
        float: right;
        color: #f05050d6;
        font-size: 20px;
        position: relative;
        bottom: 0px;
        left: 0px;
    }
    i.fa.fa-minus {
        /* float: right; */
        position: relative;
        padding: 8px;
        background: #ff5722;
        color: white;
        cursor: pointer;
    }
    .fa-plus {
    position: relative;
    padding: 8px;
    background: #8BC34A;
    color: white;
    cursor: pointer;
    }
    i#add-court {
        left: 135px;
    }
    i.fa.fa-minus.remove-court {
        left: 140px;
    }


 </style>
 @endsection

@section('content')
    @section('breadcrumb','Add '.$type)
    {{-- {{dd($laboratory_test->laboratory)}} --}}
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- jquery validation -->
              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i><span style="text-transform: capitalize;" > Add {{$type}}</span>
                              <button onclick="window.history.back(1)" type="button" class="btn btn-block btn-primary btn-md col-md-2 float-sm-right">Go Back</button>
                        </div>
                        <div class="panel-body">
                          <!-- form start -->
                          <form action="{{$isEdit ? route('master_section.update',[$p_id, $type, $master->id]) : route('master_section.store',[$p_id, $type])}}" role="form" method="POST" id="quickForm" enctype="multipart/form-data">
                            @csrf
                            @if ($isEdit)
                              <input type="hidden" name="_method" value="put">
                            @endif
                              <div class="row">
                                <div class="form-group col-md-6">
                                  <label for="fee">Name</label>
                                  <input type="text" name="name" class="form-control"  placeholder="Enter name" value="{{$master->name ?? old('name')}}">
                                  <span class="text-danger">{{$errors->first('name') ?? null}}</span>
  
                                </div>
                                <div class="form-group col-md-6">
                                  <label for="fee">Price</label>
                                  <input type="text" name="price" class="form-control"  placeholder="Enter price" value="{{$master->price ?? old('price')}}">
                                  <span class="text-danger">{{$errors->first('price') ?? null}}</span>

                                </div>
                              
                              </div>
                            </div>
                              <div class="card-footer">
                                  <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                          </form>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
  
            </div>
            <!--/.col (right) -->
          </div>
        
    </div>
  <!-- ./wrapper -->
@section('page-scripts')
<!-- jquery-validation -->
<script src="{{url('')}}/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('')}}/assets/plugins/jquery-validation/additional-methods.min.js"></script>
<script src="{{url('')}}/assets/plugins/select2/js/select2.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
    
      $('.select').select2();

      $('#quickForm').validate({
        rules: {
          name: { required: true, },
          description: { required: true, },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
        }
      });

      function readURL(input, number) {
          if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview' + number).hide();
                $('#imagePreview' + number).fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
          }
        }
        $("#imageUpload1").change(function () {
            readURL(this, 1);
        });

    
  });
</script>
@endsection
@endsection