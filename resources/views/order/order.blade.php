@extends('layouts.master')

@section('title', 'Order')

@section('top-styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@endsection

@section('content')
@section('breadcrumb','Order')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i> Order
                        </div>
                        <div class="panel-body">
                            <table id="DataTable" class="table table-bordered table-hover table-responsive" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort text-center" width="5%">S.No</th>
                                        <th>Order No</th>
                                        <th>Total Amount</th>
                                        <th>Created</th>
                                        <th class="no-sort text-center" width="12%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
<!-- ./wrapper -->
@section('page-scripts')
<!-- DataTables -->
<script src="{{url('')}}/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

@endsection
@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#DataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route("order.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "order_no",
                    "defaultContent": ""
                },
                {
                    "data": "total_amount",
                    "defaultContent": ""
                },
                {
                    "data": "created_at",
                    "defaultContent": ""
                },
                {
                    "data": "order_no",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var edit = '{{route("order_details",[":id"])}}';
                        edit = edit.replace(':id', data);
                        var checked = row.status == 1 ? 'checked' : null;
                        return `
                        <a href="` + edit + `" class="text-info p-1">
                            View
                        </a>
                        `;
                    },
                },
            ],
            "drawCallback": function (settings) {
                
            },
      //scrollX:true,
        });
    });

</script>
@endsection
@endsection
