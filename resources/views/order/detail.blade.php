@extends('layouts.master')

@section('title', 'Order Details')

@section('top-styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@endsection

@section('content')
@section('breadcrumb','Order Details')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-users"> </i> Order Details
                        </div>
                        <div class="panel-body">
                            <table id="DataTable" class="table table-bordered table-hover table-responsive" width="100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort text-center" width="5%">S.No</th>
                                        <th>Order No</th>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $i = 1;
                                        // dd($order);/
                                    @endphp
                                    @foreach ($order->orders as $order_detail)
                                    {{-- {{dd($order_detail)}} --}}
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$order_detail->order_no}}</td>
                                            <td>{{$order_detail->item->name}}</td>
                                            <td>{{$order_detail->quantity}}</td>
                                            <td>{{$order_detail->amount}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
<!-- ./wrapper -->
@section('page-scripts')
<!-- DataTables -->
<script src="{{url('')}}/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{url('')}}/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

@endsection
@section('custom-script')

@endsection
@endsection
