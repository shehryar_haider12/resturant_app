<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/scan','SiteController@scan')->name('scan');

// Route::middleware(['checktable'])->group(function () {
Route::get('/','SiteController@index')->name('web_index');
Route::get('/category/{category?}','SiteController@categoryWithItems')->name('category_items');
Route::get('/item/{item}/detail','SiteController@itemDetail')->name('item.detail');

Route::group(['prefix' => 'cart'], function () {
    Route::get('/', 'CartController@cartGet')->name('cart');
    Route::get('/add/{item}', 'CartController@cartAdd')->name('cart.add');
    Route::post('/add/items', 'CartController@cartAddItems')->name('cart.add_items');
    Route::get('/update', 'CartController@cartupdate')->name('cart.update');
    Route::get('/delete/{item_id}', 'CartController@cartDelete')->name('cart.delete');
    Route::post('/store/order', 'CartController@storeOrder')->name('order.store');
});
// });

Route::group(['prefix' => 'admin'], function () {

// Route::Dashboard
Route::group(['prefix' => 'dashboard'], function () {
    
    Route::get('/', 'HomeController@dashboard')->name('dashboard');
    Route::get('/scanner', 'HomeController@scanner')->name('scanner');
    Route::get('/settings', 'HomeController@settings')->name('users.settings');
    Route::patch('/settings', 'HomeController@patch')->name('users.settings');
});

Route::resource('/category','MenuCategoryController');

Route::resource('/items','MenuItemsController');
Route::get('/section/items','MenuItemsController@sectionItems')->name('section_items');

Route::get('/tables/qr/{id}','TablesController@QR');
Route::post('/store/table','TablesController@storeTable')->name('store.table');
Route::resource('/tables','TablesController');

Route::group(['prefix' => 'sections'], function () {

    Route::get('/create', 'MasterSectionController@createSection')->name('section.create');
    Route::post('/store', 'MasterSectionController@storeSection')->name('section.store');

    Route::get('/{p_id}/{type}', 'MasterSectionController@index')->name('master_section');
    Route::get('/{p_id}/{type}/create', 'MasterSectionController@create')->name('master_section.create');
    Route::post('/{p_id}/{type}/store', 'MasterSectionController@store')->name('master_section.store');
    Route::get('/{p_id}/{type}/edit/{master}', 'MasterSectionController@edit')->name('master_section.edit');
    Route::put('/{p_id}/{type}/update/{master}', 'MasterSectionController@update')->name('master_section.update');
});

Route::group(['prefix' => 'order'], function () {
    
    Route::get('/', 'OrderController@order')->name('order');
    Route::get('/datatable', 'OrderController@orderDatatable')->name('order.datatable');
    Route::get('/details/{order_no}', 'OrderController@orderDetails')->name('order_details');
});


Auth::routes();

});
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Cache Cleared Route
Route::get('exzed/cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
   return 'cache clear';
});


