<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ItemDetail;

class Items extends Model
{
    protected $table = 'menu_items';
    protected $fillable = [
        'name',
        'image',
        'description',
        'price',
        'c_id',
        'status',
    ];

    /**
     * Get all of the sections for the Items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sections()
    {
        return $this->hasMany(ItemDetail::class, 'item_id', 'id');
    }
    public function category()
    {
        return $this->hasOne('App\Category','id','c_id');
    }
}
