<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\OrderDetail;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'user_id',
        'table_id',
        'order_no',
        'total_amount',
    ];

    /**
     * Get all of the orders for the Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(OrderDetail::class, 'order_no', 'order_no');
    }
}
