<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tables extends Model
{
    protected $table = 'tables';
    protected $primaryKey = 'id';
    protected $fillable = [
        'table_no'
    ];
}
