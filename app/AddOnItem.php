<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\OrderDetail;

class AddOnItem extends Model
{
    protected $table = 'add_on_orders';
    protected $fillable = [
        'item_id',
        'order_item_id',
        'order_no',
    ];
}
