<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait RegisterAvail 
{
    /**
     * Return Avail Request .
     *
     * @return \Illuminate\Http\Response
     */
    public function availRequest($member_id , $facilityId = null)
    {
        return $data  = [
            'facility_id'   =>  Auth::user()->id ?? $facilityId,
            'member_id'     =>  $member_id,
        ];
    }

    /**
     * Return Avail Request .
     *
     * @return \Illuminate\Http\Response
     */
    public function updateAvailCountStatus($member_detail)
    {
        $remaining_avails = $member_detail->total_avails-1;
        return $member_detail->update(['total_avails' => $remaining_avails]);
    }

    /**
     * Return Avail Request .
     *
     * @return \Illuminate\Http\Response
     */
    public function updateTransactionAvailCountStatus($transaction)
    {
        $remaining_avails = $transaction->remaining_avails-1;
        return $transaction->update(['remaining_avails' => $remaining_avails]);
    }
}