<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemAttribute extends Model
{
    protected $table = 'item_attributes';
    protected $fillable = [
        'p_id',
        'name',
        'type',
        'price',
        'status',
    ];
}
