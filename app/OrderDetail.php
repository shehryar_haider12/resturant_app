<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Items;

class OrderDetail extends Model
{
    protected $table = 'order_details';
    protected $fillable = [
        'item_id',
        'user_id',
        'quantity',
        'order_no',
        'price',
        'amount',
    ];

    /**
     * Get the item associated with the OrderDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function item()
    {
        return $this->hasOne(Items::class, 'id', 'item_id');
    }
}
