<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'menu_category';
    protected $fillable = [
        'name',
        'image',
        'status',
    ];
    
    /**
     * Get all of the items for the Category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\Items', 'c_id', 'id');
    }
}
