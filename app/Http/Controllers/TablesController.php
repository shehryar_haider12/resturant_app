<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tables;

class TablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $table = Tables::all();
        return view('tables.index',compact('table'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $isEdit = false;
        return view('tables.create',compact('isEdit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'table_no'               =>  'required|string|max:255|unique:tables'
        ]);
        Tables:: create([
            'table_no' => $request->table_no
        ]);
        return redirect('/admin/tables');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $table = Tables::find($id);
        $data = [
            'isEdit'            =>  true,
            'table'   =>  $table
        ];
        // return $laboratory_test->includeTest;
        return view('tables.create',$data);
    }

    public function QR($id)
    {
        $curl = curl_init();
        $api='https://chart.apis.google.com/chart?chs=400x400&cht=qr&choe=UTF-8&chl=';
            curl_setopt_array($curl, array(
                CURLOPT_URL => $api,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_POSTFIELDS => $id,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: image/png',
                ),
            ));
            $filename=$api.$id;
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            return $filename;
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'table_no'               =>  'required|string|max:255'
        ]);
        $data = [
            'table_no' => $request->table_no
        ];
        
        Tables::where('id',$id)
        ->update($data);
        
        return redirect('/admin/tables');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeTable(Request $request)
    {
        \Cookie::queue('table_id', $request->table_id, 3600);
        $table_id = \Cookie::get('table_id');
        if ( $table_id) {
            $response['success'] = 'This Table ID Added in Session Successfully';
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
