<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;
use App\Items;
use Auth;
use Storage;
use Hash;
use DataTables;
use Session;

class SiteController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function scan()
    {
        return view('web.scanner');
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'categories' => Category::where('status',1)->get(),
        ];
        return view('web.index',$data);
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function categoryWithItems(Request $request, $category = null)
    {
        if(!empty($category))
        {
            $data = [
                'categories'         => Category::where('status',1)->get(),
                'category_detail'    => $category,
                'category_item'      => Items::where([['status',1],['c_id',$category]])->get(),
            ];
        }
        elseif(isset($request->keyword))
        {
            // return $request->keyword;
            $data['category_item']  = Items::where('name','LIKE','%'.$request->keyword.'%')->get();
            $data['categories']     = Category::where('status',1)->get();
        }elseif (isset($request->keyword) && !empty($category)) {
            $data = [
                'categories'         => Category::where('status',1)->get(),
                'category_detail'    => $category,
                'category_item'      => Items::where([['status',1],['name','LIKE','%'.$request->keyword.'%'],['c_id',$category]])->get(),
            ];
        }
        // return $data;
        return view('web.Make_it_meal',$data);
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function itemDetail($item)
    {
        $data = [
            'item'         => Items::with('sections.items')->where('id',$item)->first(),
        ];
        // return $data;
        return view('web.radio_btn_page',$data);
    }
}
