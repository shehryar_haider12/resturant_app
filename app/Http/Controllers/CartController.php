<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Items;
use App\ItemAttribute;
use App\Order;
use App\OrderDetail;
use Cart;
use DataTables;
use Storage;
use Session;

class CartController extends Controller
{
    public function order()
    {
        return view('order.order');
    }

    public function cartGet()
    {
        $userId = \Cookie::get('table_id');
        $data = [
            'cart'  => Cart::session($userId)->getContent(),
        ];
      
        return view('web.order_summary',$data);
    }

    public function cartAdd(Items $item)
    {
        $userID = \Cookie::get('table_id');
        $cart_items = [
                'id'                => $item->id,
                'name'              => $item->name,
                'price'             => $item->price,
                'quantity'          => 1,
                'attributes'        => array(),
                'associatedModel'   => $item  
        ];
        // return $cart_items;
        Cart::session($userID)->add($cart_items);
        return redirect()->route('cart');
    }

    public function cartAddItems(Request $request)
    {
        $userID = \Cookie::get('table_id');
        // return $request->all();
        $order_item = Items::find($request->item_id[0]);
        foreach ($request->item_id as $key => $item) {
            if($order_item->id == $item)
            {
                $item_detail = $order_item;
            }else{
                $item_detail = ItemAttribute::find($item);
            }
            $cart_items = [
                'id'                => $item,
                'name'              => $item_detail->name,
                'price'             => $item_detail->price ?? 0,
                'quantity'          => 1,
                'attributes'        => array(),
                'associatedModel'   => $item_detail  

            ];
            // return $cart_items;
            Cart::session($userID)->add($cart_items);
        }
       
        return redirect()->route('cart');
    }

    public function cartDelete($item_id)
    {
        $userID = \Cookie::get('table_id');
        Cart::session($userID)->remove($item_id);
        return redirect()->route('cart');

    }

    public function storeOrder(Request $request){
        $request->validate([
            'item_id'     =>  'required|array',
            'quantity'    =>  'required|array',
            'price'       =>  'required|array',
        ]);
        $today              =   date("Ymd");
        $rand               =   strtoupper(substr(uniqid(sha1(time())),0,4));
        $order_no           =   $today . $rand;
        
        foreach ($request->item_id as $key => $item) {
            $data = [
                'order_no'  =>  $order_no,
                'item_id'   =>  $item,
                'user_id'   =>  0,
                'price'     =>  $request->price[$key],
                'amount'    =>  $request->price[$key] * $request->quantity[$key],
                'quantity'  =>  $request->quantity[$key],
            ];
            $order[] = OrderDetail::create($data);
            $userID = \Cookie::get('table_id');
            Cart::session($userID)->remove($item);
        }
        $collect_order  =   collect($order);
        // return $collect_order;
        $amount         =   $collect_order->pluck('amount')->toArray();
        // return $amount;
        $detail     = [
            'order_no'          =>  $order_no,
            'table_id'          =>  \Cookie::get('table_id'),
            'user_id'           =>  0,
            'total_amount'      =>  array_sum($amount),
        ];
        $order_details = Order::create($detail);
        \Cookie::forget('table_id');
        return view('web.order_detail',compact('order_details'));
    }
}
