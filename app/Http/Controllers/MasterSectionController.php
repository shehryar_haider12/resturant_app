<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Items;
use App\ItemAttribute;
use App\ItemDetail;
use Storage;
class MasterSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($p_id, $type)
    {
        $data = [
            'items' => ItemAttribute::where([['type',$type],['p_id','!=',0]])->get(),
            'type'  =>  $type,
            'p_id'  =>  $p_id
        ];
        return view('master_section.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($p_id, $type)
    {
        $data = [
            'isEdit'    =>  false,
            'type'      =>  $type,
            'p_id'      =>  $p_id

        ];
        return view('master_section.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$p_id, $type)
    {
        // return $request->all();
        $data = $request->validate([
            'name'          =>  'required|string|max:255',
            'price'         =>  'required',
        ]);
        $data['type'] = $type;
        $data['p_id'] = $p_id;
        $item = ItemAttribute::create($data);
        return redirect()->route('master_section',[$p_id,$type])->with('message','Master Item Added Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($p_id, $type,  ItemAttribute $master)
    {
        $data = [
            'isEdit'     =>  true,
            'master'     =>  $master,
            'type'       =>  $type,
            'p_id'  =>  $p_id

        ];

        return view('master_section.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $p_id,  $type, ItemAttribute $master)
    {
        $data = $request->validate([
            'name'          =>  'required|string|max:255',
            'price'         =>  'required',
        ]);
      
        $master->update($data);
        return redirect()->route('master_section',[$p_id,$type])->with('message','Master Item Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($p_id, $id)
    {
        //
    }

    public function createSection()
    {
        return view('master_section.create-section');
    }

    public function storeSection(Request $request)
    {
        $data = $request->validate([
            'name'          =>  'required|string|max:255',
        ]);
        $data['p_id'] = 0;
        $data['type'] = strtolower($request->name);
        $data['type'] = trim(str_replace(' ','',$data['type']));
        $item = ItemAttribute::create($data);
        return redirect()->route('master_section',$data['type']);
    }
}
