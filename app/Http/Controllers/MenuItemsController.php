<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use App\Items;
use App\ItemAttribute;
use App\ItemDetail;
use Storage;
class MenuItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Items::with(['category'])
        ->get();
        return view('items.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cat        =   Category::all();
        $sections   =   ItemAttribute::where([['status',1],['p_id',0]])->get();
        $data = [
            'isEdit'    =>  false,
            'cat'       =>  $cat,
            'sections'  =>  $sections,
        ];
        return view('items.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $request->validate([
            'name'          =>  'required|string|max:255',
            'price'         =>  'required',
            'description'   =>  'required',
          
        ]);
        $image = Storage::disk('uploads')->putFile('',$request->image);
        $item = Items:: create([
            'name' => $request->name,
            'image' => $image,
            'description' => $request->description,
            'price' => $request->price,
            'c_id' => $request->c_id
        ]);
        if($request->section_id)
        {
            foreach ($request->section_id as $key => $section) {
                $data = [
                    'item_id'       =>  $item->id,
                    'section_id'    =>  $section,
                    'is_paid'       =>  $request->is_paid[$key],
                ];
                ItemDetail::create($data);
            }
        }
        return redirect()->route('items.index')->with('message','Item Added Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item           =   Items::with('sections.singleItem')->find($id);
        // return $item;
        $sections       =   ItemAttribute::where([['status',1],['p_id',0]])->get();
    
        $cat    = Category::all();
        $data = [
            'isEdit'                =>  true,
            'cat'                   =>  $cat,
            'item'                  =>  $item,
            'sections'              =>  $sections
        ];

        return view('items.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'          =>  'required|string|max:255',
            'price'         =>  'required',
            'description'   =>  'required',
           
        ]);
        $item           =   Items::with('sections.singleItem')->find($id);
        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'c_id' => $request->c_id
        ];
        if($request->image){
            Storage::disk('uploads')->delete($item->image);
            $data['image'] = Storage::disk('uploads')->putFile('',$request->image);

        }
        $item->update($data);
        if($request->section_id)
        {
            ItemDetail::where('item_id',$item->id)->delete();
            foreach ($request->section_id as $key => $section) {
                $item_detail = [
                    'item_id'       =>  $item->id,
                    'section_id'    =>  $section,
                    'is_paid'       =>  $request->is_paid[$key],
                ];
                ItemDetail::create($item_detail);
            }
        }
        return redirect()->route('items.index')->with('message','Item Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sectionItems(Request $request)
    {
        $section_items   =   ItemAttribute::where([['status',1],['type',$request->type]])->where('p_id','!=',0)->get();
        if ($section_items) {
            $response['success']    = 'All Section Items';
            $response['items']      = $section_items;
            return response()->json($response, 200);
        } else {
            $response['error'] = 'Oops Something went wrong!';
            return response()->json($response, 409);
        }
    }
}
