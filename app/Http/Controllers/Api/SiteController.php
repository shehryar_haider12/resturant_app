<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Items;
use App\Category;
use App\Order;
use App\AddOnItem;
use App\ItemAttribute;
use App\OrderDetail;
use App\Tables;

class SiteController extends Controller
{
    public $successStatus = 200;
    public $errorStatus = 401;

    public function getTables(Request $request)
    {
        $table=Tables::where([['status',1]])->get();
        return result($table, $this->successStatus, 'All Tables');
    }

    public function getCategory(Request $request)
    {
        $category=Category::where([['status',1]])->get();
        return result($category, $this->successStatus, 'All Category');
    }
    
    public function getItems(Request $request)
    {
        $request->validate([
            'c_id'  => 'required|exists:menu_category,id'
        ]);
        $items=Items::where([['c_id',$request->c_id],['status',1]])->get();
        return result($items, $this->successStatus, 'Items Category wise');
    }

    public function getItemDetail(Request $request)
    {
        $request->validate([
            'item_id'  => 'required|exists:menu_items,id'
        ]);
        $data['main_items']      =   Items::with('sections.items')->where([['id',$request->item_id]])->first();
        $data['add_on']          =   ItemAttribute::where([['type','addon'],['p_id','!=',0]])->get();
        return result($data, $this->successStatus, 'Item Detail');
    }

    public function storeOrder(Request $request){
        // return $request;
        $request->validate([
            'table_id'      =>  'required',
            'item_id'       =>  'required|array',
            'quantity'      =>  'required',
            'amount'        =>  'required|array',
        ]);
        $today              =   date("Ymd");
        $rand               =   strtoupper(substr(uniqid(sha1(time())),0,4));
        $order_no           =   $today . $rand;
        
        foreach ($request->item_id as $key => $item) {
            $data = [
                'order_no'  =>  $order_no,
                'item_id'   =>  $item,
                'user_id'   =>  isset($request->user('api')->id) ? $request->user('api')->id : 0,
                'price'     =>  $request->amount[$key],
                'amount'    =>  $request->amount[$key] * $request->quantity[$key],
                'quantity'  =>  $request->quantity[$key],
            ];
            $order[] = OrderDetail::create($data);
        }
        $collect_order  =   collect($order);
        // return $collect_order;
        $amount         =   $collect_order->pluck('amount')->toArray();
        // return $amount;
        $detail     = [
            'order_no'          =>  $order_no,
            'table_id'          =>  $request->table_id,
            'user_id'           =>  isset($request->user('api')->id) ? $request->user('api')->id : 0,
            'total_amount'      =>  array_sum($amount),
        ];
        $order_details = Order::create($detail);

        return response()->json([
            'message'       =>  'success',
            'order'         =>  $order_details,
            'details'       =>  $order_details->orders,
        ]);
    }
}
