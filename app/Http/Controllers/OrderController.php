<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Order;
use App\OrderDetail;
use DataTables;
use Storage;

class OrderController extends Controller
{
    public function order()
    {
        return view('order.order');
    }

    public function orderDatatable()
    {
        $orders = Order::with('orders')->orderBy('created_at','desc')->select(['id','order_no','total_amount','created_at']);

        return DataTables::of($orders)->make();
    }

    public function orderDetails($order_no)
    {
        $order = Order::with('orders.item')->where('order_no',$order_no)->first();
        // return $order;
        return view('order.detail',compact('order'));
    }
}
