<?php
use App\ItemAttribute;

 function result($data, $status, $message)
 {
    return response()->json([
        "data"      =>  $data,
        "code"      =>  $status,
        "message"   =>  $message,
        "image_url" =>  url('')."/uploads/",
    ], $status);
 }

function navSections()
{
    return ItemAttribute::where([['status',1],['p_id',0]])->get();
}

function adon_items()
{
    return ItemAttribute::where([['status',1],['type','addon'],['p_id','!=',0]])->get();
}

/**
 * Get Specified Site Setting
 * @return array 
 */
//  function getSiteSetting($key)
//  {
//     $setting =  Setting::where('key',$key)->first();
//     return $setting->value;
//  }

//  /**
//  * Get Specified Site Setting
//  * @return array 
//  */
// function loginHistory($data)
// {
//    $data['datetime'] = now();
//    LoginHistory::create($data);  
// }