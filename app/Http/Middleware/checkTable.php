<?php

namespace App\Http\Middleware;

use Closure;

class checkTable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty(\Cookie::get('table_id')))
        {
            return $next($request);
        }
        return redirect()->route('scan');
    }
}
