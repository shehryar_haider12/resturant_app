<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemDetail extends Model
{
    protected $table = 'item_detail';
    protected $fillable = [
        'item_id',
        'section_id',
        'is_paid',
    ];

    /**
     * Get all of the items for the ItemDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(ItemAttribute::class, 'p_id', 'section_id');
    }

    /**
     * Get all of the items for the ItemDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function singleItem()
    {
        return $this->hasOne(ItemAttribute::class, 'id', 'section_id');
    }
}
